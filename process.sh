#!/bin/bash

# this script runs alignment production for a given directory

if [ $# -lt 2 ] ; then
	exit 1
fi

NUMPROC=`LANG=C cat /proc/cpuinfo | egrep "^processor.*:" | wc -l`
BASEDIR=$PWD

TSPACE=126
INSRATE=0.8
DELRATE=0.1333
MISRATE=0.0666
ERATE=0.15
ERATEHIGH=0.40
STDDEV=0.03
STDDEVHIGH=0.03
RANDLEN=0
MINLEN=1000
NUMTRAV=20
LONGREADGENOPTIONS="insrate=${INSRATE} delrate=${DELRATE} substrate=${MISRATE}  randomseed=12344321 \
  eratelow=${ERATE} eratehigh=${ERATEHIGH} eratelowstddev=${STDDEV} eratehighstddev=${STDDEVHIGH} \
  randlen=${RANDLEN} minlen=${MINLEN} numtraversals=${NUMTRAV} placerandom=0"

DIREC=$1
KV=$2

set -Eeuxo pipefail

source mod_process.sh

processDirectory ${DIREC} ${KV}
