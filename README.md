# Benchmarking scripts for daccord's agreement (split_agr) and disagreement (split_dis) based haplotype/repeat separation (see https://www.biorxiv.org/content/early/2017/06/02/145474)

The required software can be installed using the install_daccord.sh bash
script. This will also download the required genomic data.

The production of the required alignment files can then be started using

	make -f install_mk all

This will (amongst other files) generate make files for running split_agr
and split_dis in various settings. The names of these make files can be
listed using

	find genomes -name \*mk

after the alignment files have been produced. Each of these make files
should be processed by changing into the respective directory and calling
the last target in the make file. For instance we have the make file

	genomes/ecoli_spiked_1/split_agr_mk

with the last target being split_agr.las.cleanup.cleanup, so one needs to run

	cd genomes/ecoli_spiked_1 && make -f split_agr_mk split_agr.las.cleanup.cleanup

Alternatively on a SLURM based HPC system one can use hpcsched (see https://gitlab.com/german.tischler/hpcsched) to process
the files after running

	bash generatemakefiles.sh

This will produces

  * FreeBayes (see https://github.com/ekg/freebayes) VCF files
    in the directories genomes/ecoli_spiked_1 and genomes/fcgr
  * The files split_agr.las and split_disfull.las in the directories
    genomes/ecoli_spiked_[1-8] and genomes/fcgr

For producing whatshap (see https://whatshap.readthedocs.io/en/latest/)
based benchmarks please first run

	bash callsplitvcf.sh

and then

	bash callwhatshap.sh genomes/fcgr/reads_daligner_longest.bam genomes/fcgr/reads_daligner.vcf
	bash callwhatshap.sh genomes/fcgr/reads_daligner_longest.bam genomes/fcgr/split_agr.las.vcf
	bash callwhatshap.sh genomes/ecoli_spiked_1/reads_daligner_longest.bam genomes/ecoli_spiked_1/reads_daligner.vcf
	bash callwhatshap.sh genomes/ecoli_spiked_1/reads_daligner_longest.bam genomes/ecoli_spiked_1/split_agr.las.vcf

After all these files have been produced an evaluation can be triggered
using

	bash evaluate.sh
