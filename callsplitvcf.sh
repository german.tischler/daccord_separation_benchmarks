#!/bin/bash

# generate VCF from agreement based in splitting in ecoli_spiked_1 and fcgr

for i in genomes/ecoli_spiked_1 genomes/fcgr ; do
	pushd ${i}
	../../tools/bin/split_agr --call -Ereads.eprof -d20 -wreads.wmap.fasta < split_agr.las.log > split_agr.las.vcf
	popd
done
