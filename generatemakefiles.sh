#!/bin/bash
PATH=/projects/dazzlerDevel/tischler/software/hpcsched/current/x86_64-debian6_eglibc_2_11-linux-gnu/bin/:$PATH
export PATH

find genomes/ | grep hpc | xargs rm -fR
SCHEDOPT="--workers1024 -t1"

MAKEFILE=Makefile

function makeit
{
	printf "#{{hpcschedflags}} {{deepsleep}} {{mem10000}}\n"
	O=$PWD
	rm -f makelist
	for i in `find genomes -name \*mk | sort` ; do
		B=`dirname ${i}`
		F=`basename ${i}`
		cd ${B}
		CDL=`hpcschedmake ${F}`
		printf "${i}:\n\tcd ${B} && hpcschedcontrol ${SCHEDOPT} --workermem10000 --workertime1440 -pbatch ${CDL}\n"
		cd ${O}

		echo ${i} >>makelist
	done
	
	printf "all:"
	for i in `cat makelist` ; do
		printf " ${i}"
	done
	printf "\n\techo\n"
}

makeit >${MAKEFILE}
hpcschedmake ${MAKEFILE} >${MAKEFILE}.cdl
printf "#/bin/bash\nPATH=/projects/dazzlerDevel/tischler/software/hpcsched/current/x86_64-debian6_eglibc_2_11-linux-gnu/bin/:\$PATH\nexport PATH\nhpcschedcontrol --workertime$((1440)) -pbatch --workermem4000 --workers8 -t1 `cat ${MAKEFILE}.cdl` >${MAKEFILE}.log 2>&1 </dev/null &\n" >runit_${MAKEFILE}.sh
