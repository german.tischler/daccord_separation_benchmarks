#!/bin/bash

# run whatshap given a BAM file and a VCF file

# note that this splits the BAM and VCF file according to ref seq below
# while this is coincidentally also a way to facilitate parallelism
# the real reason for this is that whatshap cannot currently handle
# settings where alignments in the BAM file exist for a ref seq which
# has no variants given in the VCF file

if [ $# -lt 2 ] ; then
	exit 1
fi

export BAM=$1
export VCF=$2

export HAPLOBAM=${VCF%.vcf}_haplo.bam

export TMPPREFIX=tmp_`hostname`_$$
export EXPLODELIST=${TMPPREFIX}.explode
export VCFLIST=${TMPPREFIX}.vcflist
export WHATSHAPVCF=${TMPPREFIX}.vcf.whatshap
export PHASELOGLIST=${TMPPREFIX}.phaseloglist
export MERGEPHASESCRIPT=${TMPPREFIX}_mergephase.sh
export MK=${TMPPREFIX}_makefile
export HAPLOOUTLIST=${TMPPREFIX}.haploout
export HAPLOLOGLIST=${TMPPREFIX}.haplolog
export HAPLOMERGESCRIPT=${TMPPREFIX}.haplomerge

../../tools/bin/bamexplode level=0 sizethres=1 < ${BAM} prefix=${TMPPREFIX}_explode 2>&1 | egrep "\[O\]" | sed "s|\[O\]	||" \
	>${EXPLODELIST}

function createmk
{
rm -f ${VCFLIST}
rm -f ${PHASELOGLIST}
for i in `cat ${EXPLODELIST}` ; do
	BAMFN=${i}
	OUTFN=${i}.vcf
	LOG=${i}.log
	REF=`../../tools/bin/samtools view ${BAMFN} | awk '{print $3}' | head -n 1`
	SUBVCF=${i}.subvcf
	SCRIPT=${i}.sh
cat <<EOF >${SCRIPT}
	egrep "^(#|${REF})" <$VCF >${SUBVCF} || echo "No match" >/dev/null
	samtools index ${BAMFN}
	/usr/bin/time ../../tools/bin/whatshap phase --indels --reference reads.wmap.fasta ${SUBVCF} ${BAMFN} >${OUTFN} 2>${LOG}
	rm -f ${SUBVCF}
EOF
	echo ${OUTFN} >>${VCFLIST}
	echo ${LOG} >>${PHASELOGLIST}

	printf "${OUTFN}:\n\tbash ${SCRIPT}\n"
done

FIRSTVCF=`head -n 1 ${VCFLIST}`

cat <<EOF >${MERGEPHASESCRIPT}
rm -f ${WHATSHAPVCF}
egrep "^#" <${FIRSTVCF} >${WHATSHAPVCF}
cat ${VCFLIST} | xargs cat | egrep -v "^#" >>${WHATSHAPVCF}
cat ${VCFLIST} | xargs rm -f
rm -f ${VCFLIST}
cat ${PHASELOGLIST} | xargs cat >${VCF}.phase.log
cat ${PHASELOGLIST} | xargs rm -f
rm -f ${PHASELOGLIST}

EOF

printf "${WHATSHAPVCF}:"
for i in `cat ${VCFLIST}` ; do
	printf " ${i}"
done
printf "\n"
printf "\tbash ${MERGEPHASESCRIPT}\n"

rm -f ${HAPLOOUTLIST}
rm -f ${HAPLOLOGLIST}
for i in `cat ${EXPLODELIST}` ; do
	BAMFN=${i}
	OUTFN=${i%.bam}_haplo.bam
	LOG=${i%.bam}_haplo.log
	SUBVCF=${i}.haplosubvcf
	SCRIPT=${i}.haplosh
	REF=`../../tools/bin/samtools view ${BAMFN} | awk '{print $3}' | head -n 1`
cat <<EOF >${SCRIPT}
	egrep "^(#|${REF})" <${WHATSHAPVCF} >${SUBVCF} || echo "No match" >/dev/null
	samtools index ${BAMFN}
	/usr/bin/time ../../tools/bin/whatshap haplotag --reference reads.wmap.fasta ${SUBVCF} ${i} >${OUTFN} 2>${LOG}
	rm -f ${SUBVCF}
	rm -f ${BAMFN}
	rm -f ${BAMFN}.bai
	rm -f ${BAMFN%.bam}.bai
EOF
	printf "${OUTFN}: ${WHATSHAPVCF}\n"
	printf "\tbash ${SCRIPT}\n"
	
	echo ${OUTFN} >>${HAPLOOUTLIST}
	echo ${LOG} >>${HAPLOLOGLIST}
done

cat <<EOF >${HAPLOMERGESCRIPT}
../../tools/bin/bammerge level=0 IL=${HAPLOOUTLIST} | ../../tools/bin/bamsormadup threads=1 >${HAPLOBAM}
cat ${HAPLOOUTLIST} | xargs rm -f
rm -f ${HAPLOOUTLIST}
cat ${HAPLOLOGLIST} | xargs cat >${HAPLOBAM%.bam}.log
cat ${HAPLOLOGLIST} | xargs rm -f
rm -f ${HAPLOLOGLIST}

cat ${EXPLODELIST} | xargs rm -f
rm ${EXPLODELIST}
rm -f ${TMPPREFIX}
EOF

printf "${HAPLOBAM}:"
for i in `cat ${HAPLOOUTLIST}` ; do
	printf " ${i}"
done
printf "\n"
printf "\tbash ${HAPLOMERGESCRIPT}\n"
}

createmk >${MK}
make -j24 -f${MK} ${HAPLOBAM}
