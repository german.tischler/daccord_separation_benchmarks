#! /bin/bash

# this file contains module functions for install_daccord.sh

function cleanlang
{
	LCT=`set | grep ^LC_ | sed "s|=.*||"`

	if [ ! -z "${LCT}" ] ; then
		for i in ${LCT} ; do
			unset ${LCT}
			export ${LCT}
		done
	fi

	unset LCT

	set | grep LC_

	export LANG=C
}

function rundaligner
{
	local DB=$1
	local RDIR=$PWD
	
	cd `dirname $DB`
	
	local LDB=`basename ${DB}`
	
	local LNUMBLOCKS=`cat ${LDB} | grep "blocks =" | awk '{print $3}'`
	
	for i in `seq 1 ${LNUMBLOCKS}` ; do
		for j in `seq ${i} ${LNUMBLOCKS}` ; do
			PATH=${BASEDIR}/tools/bin:$PATH ${BASEDIR}/tools/bin/daligner -s${TSPACE} -M30 -T${NUMPROC} ${LDB%.db}.${i} ${LDB%.db}.${j}
		done
	done
	
	local LIST=list
	rm -f ${LIST}
	touch ${LIST}

	for i in `seq 1 ${LNUMBLOCKS}` ; do
		for j in `seq 1 ${LNUMBLOCKS}` ; do
			echo ${LDB%.db}.${i}.${LDB%.db}.${j}.las >>${LIST}
		done
	done
	
	${BASEDIR}/tools/bin/lassort2 -l ${LDB%.db}.las ${LIST}
	cat ${LIST} | xargs rm -f
	
	cd $RDIR
}

function generateReads
{
	${BASEDIR}/tools/bin/longreadgen ${LONGREADGENOPTIONS} \
		text.fasta reads.fasta > reads.bam 2>reads.log 

	# sort reads file
	${BASEDIR}/tools/bin/bamsort < reads.bam > reads_s.bam index=1 indexfilename=reads_s.bam.bai
	# create numerical index
	${BASEDIR}/tools/bin/bamnumericalindex numerical=reads.bam.numindex <reads.bam

	# generate perfect piles
	${BASEDIR}/tools/bin/generateperfectpiles -t${NUMPROC} --tspace${TSPACE} reads_gen.las reads_s.bam
}

function preprocesslas
{
	# preprocess reads for split_agr and split_dis
        ${BASEDIR}/tools/bin/computeintrinsicqv2 -d${NUMTRAV} reads.db reads.las
        ${BASEDIR}/tools/bin/lasdetectsimplerepeats -d$((NUMTRAV / 2)) reads.rep reads.db reads.las
        ${BASEDIR}/tools/bin/lasfilteralignments reads_filtered.las reads.db reads.las
        ${BASEDIR}/tools/bin/filtersym reads_filtered.las reads_filtered.las.sym
        rm reads_filtered.las.sym
	${BASEDIR}/tools/bin/lasfilteralignmentsborderrepeats reads_filtered_border.las reads.db reads.rep reads_filtered.las
	${BASEDIR}/tools/bin/filtersym reads_filtered_border.las reads_filtered_border.las.sym
	rm reads_filtered_border.las.sym
}

function runconsensus
{
	${BASEDIR}/tools/bin/daccord --vard20 -Ereads.eprof --eprofonly reads_filtered_border.las reads.db
	${BASEDIR}/tools/bin/daccord --vard20 -Ereads.eprof reads_filtered_border.las reads.db >reads.dac.fasta
}

function runexqv
{
	${BASEDIR}/tools/bin/computeextrinsicqv --tspace${TSPACE} reads.dac.fasta reads.db
}

function runFilterChains
{
	${BASEDIR}/tools/bin/filterchains -l5000 reads_filtered_border_chain.las reads.dac.fasta reads_filtered_border.las
}

# callSplit dis disfull reads_filtered_border_chain.las --kv2 --fulltuples
function callSplit
{
	local TYP=$1
	shift
	local FTYP=$1
	shift
	local INPUT=$1
	shift
	
	SPLIT_MK=$PWD/split_${FTYP}_mk
	rm -f ${SPLIT_MK}
	touch ${SPLIT_MK}
	
	printf "#{{hpcschedflags}} {{deepsleep}} {{mem10000}}\n" >>${SPLIT_MK}
	for i in `seq 0 $((PARTS - 1))` ; do
		printf "split_${FTYP}_${i}.las:\n" >>${SPLIT_MK}
		printf "\t${BASEDIR}/tools/bin/split_${TYP} $* -Ereads.eprof -J${i},${PARTS} -d$((NUMTRAV)) -t1 split_${FTYP}_${i}.las reads.dac.fasta ${INPUT} reads.db >split_${FTYP}_${i}.las.log 2>&1 </dev/null\n" >>${SPLIT_MK}
	done
	
	NPARTS=${PARTS}
	NDEPTH=0
	FOUTFN=split_${FTYP}_0.las
	FOUTDROPFN=split_${FTYP}_0_drop.las
	while [ ${NPARTS} -gt 1 ] ; do
		OUTN=$(((NPARTS + 1) /2))
	
		if [ $NDEPTH -eq 0 ] ; then
			for i in `seq 0 $((OUTN -1))` ; do
				CLEANFN=""
				INFN=split_${FTYP}_$((i * 2 + 0)).las
				if [ $((i * 2 + 1)) -lt ${NPARTS} ] ; then
					INFN="${INFN} split_${FTYP}_$((i * 2 + 1)).las"
				fi
				OUTFN=split_${FTYP}_$((NDEPTH))_${i}.las
				CLEANFN="${CLEANFN} ${INFN}"
				
				printf "${OUTFN}: ${INFN}\n" >>${SPLIT_MK}
				printf "\t${BASEDIR}/tools/bin/lassort2 -t1 ${OUTFN} ${INFN}\n" >>${SPLIT_MK}

				INFN=split_${FTYP}_$((i * 2 + 0))_drop.las
				if [ $((i * 2 + 1)) -lt ${NPARTS} ] ; then
					INFN="${INFN} split_${FTYP}_$((i * 2 + 1))_drop.las"
				fi
				OUTFN=split_${FTYP}_$((NDEPTH))_${i}_drop.las
				CLEANFN="${CLEANFN} ${INFN}"
				
				printf "\t${BASEDIR}/tools/bin/lassort2 -t1 ${OUTFN} ${INFN}\n" >>${SPLIT_MK}

				printf "split_${FTYP}_$((NDEPTH))_${i}.las.cleanup: split_${FTYP}_$((NDEPTH))_${i}.las\n" >>${SPLIT_MK}
				printf "\trm -f ${CLEANFN}\n" >>${SPLIT_MK}
			done
		else
			for i in `seq 0 $((OUTN -1))` ; do				
				CLEANFN=""
				INFN=split_${FTYP}_$((NDEPTH - 1))_$((i * 2 + 0)).las
				if [ $((i * 2 + 1)) -lt ${NPARTS} ] ; then
					INFN="${INFN} split_${FTYP}_$((NDEPTH - 1))_$((i * 2 + 1)).las"
				fi
				OUTFN=split_${FTYP}_$((NDEPTH))_${i}.las
				CLEANFN="${CLEANFN} ${INFN}"
				
				printf "${OUTFN}: ${INFN}\n" >>${SPLIT_MK}
				printf "\t${BASEDIR}/tools/bin/lassort2 -t1 ${OUTFN} ${INFN}\n" >>${SPLIT_MK}

				INFN=split_${FTYP}_$((NDEPTH - 1))_$((i * 2 + 0))_drop.las
				if [ $((i * 2 + 1)) -lt ${NPARTS} ] ; then
					INFN="${INFN} split_${FTYP}_$((NDEPTH - 1))_$((i * 2 + 1))_drop.las"
				fi
				OUTFN=split_${FTYP}_$((NDEPTH))_${i}_drop.las
				CLEANFN="${CLEANFN} ${INFN}"
				
				printf "\t${BASEDIR}/tools/bin/lassort2 -t1 ${OUTFN} ${INFN}\n" >>${SPLIT_MK}
				
				printf "split_${FTYP}_$((NDEPTH))_${i}.las.cleanup: split_${FTYP}_$((NDEPTH))_${i}.las\n" >>${SPLIT_MK}
				printf "\trm -f ${CLEANFN}\n" >>${SPLIT_MK}
			done
		fi
		
		FOUTFN=split_${FTYP}_$((NDEPTH))_0.las
		FOUTDROPFN=split_${FTYP}_$((NDEPTH))_0_drop.las
		NDEPTH=$((NDEPTH + 1))
		NPARTS=${OUTN}
	done

	SPLITINDEX=split_${FTYP}_index.list
	SPLITLOG=split_${FTYP}_log.list
	SPLITDROPINDEX=split_${FTYP}_drop_index.list
	rm -f ${SPLITDROPINDEX}
	touch ${SPLITDROPINDEX}
	rm -f ${SPLITINDEX}
	touch ${SPLITINDEX}
	rm -f ${SPLITLOG}
	touch ${SPLITLOG}
	for i in `seq 0 $((PARTS - 1))` ; do
		echo .split_${FTYP}_${i}.las.bidx >>${SPLITINDEX}
		echo split_${FTYP}_${i}.las.log >>${SPLITLOG}
		echo .split_${FTYP}_${i}_drop.las.bidx >>${SPLITDROPINDEX}
	done

	printf "split_${FTYP}.las: ${FOUTFN}\n" >>${SPLIT_MK}
	printf "\t${BASEDIR}/tools/bin/lassort2 -t1 split_${FTYP}.las ${FOUTFN}\n" >>${SPLIT_MK}
	printf "\t${BASEDIR}/tools/bin/lassort2 -t1 split_${FTYP}_drop.las ${FOUTDROPFN}\n" >>${SPLIT_MK}
	printf "\t(cat ${SPLITLOG} | xargs cat) >split_${FTYP}.las.log\n" >>${SPLIT_MK}

	printf "split_${FTYP}.las.cleanup: split_${FTYP}.las\n" >>${SPLIT_MK}	
	printf "\trm -f ${FOUTFN}\n" >>${SPLIT_MK}
	printf "\trm -f ${FOUTDROPFN}\n" >>${SPLIT_MK}
	printf "\tcat ${SPLITINDEX} | xargs rm -f\n" >>${SPLIT_MK}
	printf "\tcat ${SPLITDROPINDEX} | xargs rm -f\n" >>${SPLIT_MK}
	printf "\tcat ${SPLITLOG} | xargs rm -f\n" >>${SPLIT_MK}
	
	printf "split_${FTYP}.las.cleanup.cleanup: split_${FTYP}.las.cleanup\n" >>${SPLIT_MK}
	printf "\trm -f ${SPLITINDEX} ${SPLITDROPINDEX} ${SPLITLOG}\n" >>${SPLIT_MK}

}

function processDirectory
{
	local DIREC=$1
	local KV=$2
	
	cleanlang

	pushd ${DIREC}

	# normalise and index
	${BASEDIR}/tools/bin/normalisefasta toupper=1 \
		< text.fasta \
		> text.fasta.tmp \
		2>text.fasta.fai
	mv text.fasta.tmp text.fasta
	
	{ time generateReads ; } >generateReads.log 2>&1 </dev/null
		 
	PARTS=`grep ">" reads.fasta | wc -l`
		 
	# compute daligner alignments
	${BASEDIR}/tools/bin/fasta2DB reads.db reads.fasta
	${BASEDIR}/tools/bin/DBsplit -s200 -x0 reads.db
	{ time rundaligner reads.db ; } >daligner.log 2>&1 </dev/null
	
	${BASEDIR}/tools/bin/checklas --mark reads.db reads_gen.las reads.bam reads.las
        mv reads.las.mark.las reads.las
        rm .reads.las.mark.las.bidx

	{ time preprocesslas ; } > preprocesslas.log 2>&1 </dev/null
	{ time runconsensus ; } > consensus.log 2>&1 </dev/null
	{ time runexqv ; } > exqv.log 2>&1 </dev/null

	${BASEDIR}/tools/bin/wmap reads.wmap.las reads.db reads.fasta reads.dac.fasta reads.las >reads.wmap.fasta
	${BASEDIR}/tools/bin/fasta2DB reads.wmap.db reads.wmap.fasta
	${BASEDIR}/tools/bin/DBsplit -x0 -s200 reads.wmap.db
	
	${BASEDIR}/tools/bin/rlastobam -t${NUMPROC} reads.wmap.db reads.db reads.fasta reads.wmap.las | \
		${BASEDIR}/tools/bin/bamsormadup threads=${NUMPROC} > reads_daligner.bam
	${BASEDIR}/tools/bin/bamfilterlongest < reads_daligner.bam | ${BASEDIR}/tools/bin/bamsormadup threads=${NUMPROC} indexfilename=reads_daligner_longest.bam.bai > reads_daligner_longest.bam
	${BASEDIR}/tools/bin/bamexplode level=0 sizethres=1 < reads_daligner_longest.bam prefix=reads_daligner_explode_ 2>&1 | egrep "\[O\]" | sed "s|\[O\]	||" >explode.list
	${BASEDIR}/tools/bin/samtools faidx reads.fasta

	#${BASEDIR}/tools/bin/wmap x.las reads.db reads.fasta reads.dac.fasta reads.las >x.fasta
	#${BASEDIR}/tools/bin/fasta2DB x.db x.fasta 
	#${BASEDIR}/tools/bin/DBsplit -s200 -x0 x.db 
	#${BASEDIR}/tools/bin/rlastobam -t${NUMPROC} x.db reads.db reads.fasta x.las | ${BASEDIR}/tools/bin/bamsormadup threads=${NUMPROC} > x.bam
	#${BASEDIR}/tools/bin/bamfilterlongest < x.bam | ${BASEDIR}/tools/bin/bamsormadup threads=${NUMPROC} indexfilename=x_longest.bam.bai > x_longest.bam

	FIRSTBAM=`head -n 1 explode.list`
	FIRSTVCF=${FIRSTBAM}.vcf

	if [ ${KV} -le 2 ] ; then
		FREEBAYESMK=fbmk
		ALL=
		rm -f ${FREEBAYESMK}
		touch ${FREEBAYESMK}
		rm -f ${FREEBAYESMK}.list
		touch ${FREEBAYESMK}.list
		rm -f ${FREEBAYESMK}.loglist
		touch ${FREEBAYESMK}.loglist
		printf "#{{hpcschedflags}} {{deepsleep}} {{mem10000}}\n" >>${FREEBAYESMK}
		for i in `cat explode.list` ; do
			VCF="${i}.vcf"
			echo ${VCF} >> ${FREEBAYESMK}.list
			echo ${VCF}.log >> ${FREEBAYESMK}.loglist
			ALL="$ALL ${VCF}"
			printf "${VCF}:\n\t/usr/bin/time ${BASEDIR}/tools/bin/freebayes --ploidy ${KV} -f reads.wmap.fasta ${i} >${VCF} 2>${VCF}.log\n" >>${FREEBAYESMK}
			printf "${VCF}.cleanup: ${VCF}\n\trm ${i}\n" >>${FREEBAYESMK}
		done
		printf "reads_daligner.vcf: ${ALL}\n" >>${FREEBAYESMK}
		# logs
		printf "\tcat ${FREEBAYESMK}.loglist | xargs cat > reads_daligner.vcf.log\n" >>${FREEBAYESMK}
		# VCF header
		printf "\tgrep \"#\" ${FIRSTVCF} >reads_daligner.vcf\n" >>${FREEBAYESMK}
		# VCF data
		printf "\tcat ${FREEBAYESMK}.list | xargs cat | grep -v \"#\" >>reads_daligner.vcf\n\tcat ${FREEBAYESMK}.list | xargs rm -f\n" >>${FREEBAYESMK}
		# cleanup
		printf "reads_daligner.vcf.cleanup: reads_daligner.vcf\n" >>${FREEBAYESMK}
		printf "\trm ${FREEBAYESMK}.list explode.list\n" >>${FREEBAYESMK}
		printf "\tcat ${FREEBAYESMK}.loglist | xargs rm -f\n" >>${FREEBAYESMK}
	fi

	# XZ	

	#
	{ time runFilterChains ; } >runFilterChains.log 2>&1 </dev/null

	callSplit agr agr reads_filtered_border_chain.las
	callSplit dis dis reads_filtered_border_chain.las --kv${KV}
	callSplit dis disfull reads_filtered_border_chain.las --kv${KV} --fulltuples
	
	# make -j${NUMPROC} -f${SPLIT_AGR_MK} split_agr.las
	# make -j${NUMPROC} -f${SPLIT_DIS_MK} split_dis.las
	# make -j${NUMPROC} -f${FREEBAYESMK} reads_daligner.vcf

	# exit 0

        popd

}
