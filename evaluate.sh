#!/bin/bash

# this script evaluates the log files produced and produces a table
# containing precision,recall,fscore,runtime

TMP=tmp_$$

function evaluate_daccord_las
{
	LAS=$1
	DIR=`dirname ${LAS}`
	DB=$DIR/reads.db
	NR=`tools/bin/lasshow ${DB} ${DB} ${LAS} | awk '{A=$1; B=$2; gsub(/\[.*/,"",A); gsub(/\[.*/,"",B); gsub(/[cn]/,"",B); print A }' | uniq | sort -n -u | wc -l`
	DROP=${LAS%.las}_drop.las
	LOG=${LAS}.log
	TIME=`awk < ${LOG} 'BEGIN{S=0};/accumulated/{S+=$4};END{printf("%.2f\n",S)}'`
	tools/bin/evalsplit ${LAS} ${DROP} >${TMP}
	PRECISION=`awk < ${TMP} '/precision/{print $2}'`
	RECALL=`awk < ${TMP} '/recall/{print $2}'`
	FSCORE=`awk < ${TMP} '/fscore/{print $2}'`
	#echo ${LAS%.las} ${PRECISION} ${RECALL} ${FSCORE} ${TIME}
	awk -vNR=${NR} -vLAS=${LAS%.las} -vPRECISION=${PRECISION} -vRECALL=${RECALL} -vFSCORE=${FSCORE} -vTIME=${TIME} \
		'END{printf("%s\t%.3f\t%.3f\t%.3f\t%d\t%d\t%.2f\n",LAS,PRECISION,RECALL,FSCORE,TIME+0.5,NR,TIME/NR)}' </dev/null
}

function evaluate_daccord
{
	local FN=$1
	for i in genomes/ecoli_spiked_* ; do
		if [ ! -e $i/${FN} ] ; then
			echo "Missing $i/${FN}"
			ls ${i}/${FN%.las}_*.las | grep -v drop >${TMP}_nondrop
			ls ${i}/${FN%.las}_*.las | grep drop >${TMP}_drop
			ls ${i}/${FN%.las}_*.log >${TMP}_log
			# set -x
			tools/bin/lassort2 -M16M -t24 -l ${i}/${TMP}.las ${TMP}_nondrop 2>/dev/null
			tools/bin/lassort2 -M16M -t24 -l ${i}/${TMP}_drop.las ${TMP}_drop 2>/dev/null
			cat ${TMP}_log | xargs cat > ${i}/${TMP}.las.log
			evaluate_daccord_las ${i}/${TMP}.las
			rm -f ${TMP}_nondrop ${TMP}_drop ${i}/${TMP}.las ${i}/${TMP}_drop.las ${i}/${TMP}.las.log
			# set +x
		fi
	done

	for LAS in `find genomes -name ${FN} | sort` ; do
		evaluate_daccord_las ${LAS}
	done
	
}

function evaluate_splitwhatshap
{
	local FN=$1
	for BAM in `find genomes -name ${FN}` ; do
		DIR=`dirname ${BAM}`
		DB=$DIR/reads.db
		NR=`samtools view ${BAM} | awk '{print $3}' | uniq | sort -u | wc -l`
		#echo $BAM
		
		HAPLOLOG=${BAM%.bam}.log
		PHASELOG=${BAM%_haplo.bam}.vcf.phase.log

		tools/bin/bamevalwhatshap <${BAM} >${TMP}
		PRECISION=`awk < ${TMP} '/precision/{print $2}'`
		RECALL=`awk < ${TMP} '/recall/{print $2}'`
		FSCORE=`awk < ${TMP} '/fscore/{print $2}'`
				
		TIME=`egrep "system.*elapsed" ${HAPLOLOG} ${PHASELOG} | while read line ; do
			echo $line | tools/bin/parsetime
		done | awk 'BEGIN{S=0};{S+=$1};END{printf("%d\n",S+0.5)}'`
		
		# echo $BAM $TIME

		awk -vNR=${NR} -vLAS=${BAM} -vPRECISION=${PRECISION} -vRECALL=${RECALL} -vFSCORE=${FSCORE} -vTIME=${TIME} \
			'END{printf("%s\t%.3f\t%.3f\t%.3f\t%d\t%d\t%.2f\n",LAS,PRECISION,RECALL,FSCORE,TIME+0.5,NR,TIME/NR)}' </dev/null
		
		# echo ${BAM} ${HAPLOLOG} ${PHASELOG}
	done
}

function calleval
{
evaluate_splitwhatshap reads_daligner_haplo.bam
evaluate_splitwhatshap split_agr.las_haplo.bam

evaluate_daccord split_agr.las
evaluate_daccord split_dis.las
evaluate_daccord split_disfull.las
}

ECNT=0
EFILE=evaluate_${ECNT}.log

while [ -e ${EFILE} ] ; do
	ECNT=$((ECNT + 1))
	EFILE=evaluate_${ECNT}.log
done

calleval | tee ${EFILE}

rm -f ${TMP}
